/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Sebastian
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Result {
    
    private Integer antecedenteAlergias;
    private AntecedenteFamiliar antecedenteFamiliar;
    private Integer condicion;
    private Diagnostico diagnostico;
    private DiagnosticoComplicacion diagnosticoComplicacion;
    private DiagnosticoMuerte diagnosticoMuerte;
    private DiagnosticoRelacionado1 diagnosticoRelacionado1;
    private DiagnosticoRelacionado2 diagnosticoRelacionado2;
    private DiagnosticoRelacionado3 diagnosticoRelacionado3;
    private Integer diasIncapacidad;
    private Integer diasMaternidad;
    private FactorRiesgo factorRiesgo;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date fechaFinalizacion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date fechaValoracion;
    private String instrumento;
    private String parametro;
    private String planTratamiento;
    private PrestadorRefiere prestadorRefiere;
    private Profesional profesional;
    private Integer tipoDiagnostico;
    private Integer tipoIncapacidad;
    private Integer valor;
    
}
