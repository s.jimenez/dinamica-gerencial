/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.interoperabilidad.salud.conectordinamicagerencial.repository.sqlserver;

import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.EdadEstimada;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.Eps;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.Identificacion;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.IdentificationPatient;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.MunicipioResidencia;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.Nacionalidad;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.Nombres;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.Ocupacion;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.PaisResidencia;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.VoluntadAnticipada;
import java.sql.ResultSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sebastian
 */
@Repository
public class IdentificationPatientRepository {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    public IdentificationPatient getInfoContacto(Integer id) {
        IdentificationPatient identificationPatient = this.jdbcTemplate.queryForObject("SELECT  \n"
                + "	( Case When (SELECT P.GPACODIGO FROM  DGEMPRES60..GENPAISES AS P WHERE B.GENPAIS=P.OID) IS NULL  Then 'COL' ELSE ---Esto se deja asi pero deberia ser un join \n"
                + " 	(SELECT P.GPACODIGO FROM  DGEMPRES60..GENPAISES AS P WHERE B.GENPAIS=P.OID)  END ) AS CODIGO_NACIONALIDAD,\n"
                + " 	( Case When (SELECT P.GPANOMBRE FROM  DGEMPRES60..GENPAISES AS P WHERE B.GENPAIS=P.OID) IS NULL  Then 'COLOMBIA' ELSE\n"
                + " 	(SELECT P.GPANOMBRE FROM  DGEMPRES60..GENPAISES AS P WHERE B.GENPAIS=P.OID)  END ) AS NACIONALIDAD,\n"
                + "	( Case When  B.PACTIPDOC = 1 Then 'CC'\n"
                + "		When  B.PACTIPDOC= 2 Then 'CE'\n"
                + "		When  B.PACTIPDOC= 3 Then 'TI'\n"
                + "		When  B.PACTIPDOC= 4 Then 'RC'\n"
                + "		When  B.PACTIPDOC= 5 Then 'PA'\n"
                + "		When  B.PACTIPDOC= 6 Then 'AS'\n"
                + "		When  B.PACTIPDOC= 7 Then 'MS'\n"
                + "		When  B.PACTIPDOC= 8 Then 'NUI'\n"
                + "		When  B.PACTIPDOC= 9 Then 'SC'\n"
                + "		When  B.PACTIPDOC= 10 Then 'CN'\n"
                + "		When  B.PACTIPDOC= 11 Then 'CD'\n"
                + "		When  B.PACTIPDOC= 12 Then 'PE'\n"
                + "	End) As IDENTIFICACION, --Tipo de documento de identificacion del usuario\n"
                + "	B.PACNUMDOC DOCUMENTO,  --Numero de documento de identificacion del usuario\n"
                + "	B.PACPRIAPE APELLIDO1,B.PACSEGAPE APELLIDO2,B.PACPRINOM NOMBRE1,B.PACSEGNOM NOMBRE2,\n"
                + "	B.GPAFECNAC,\n"
                + "	datediff(day,B.GPAFECNAC,getdate())/365 as EDAD,--ARREGLAR TIPO FECHA\n"
                + "		\n"
                + "	(Case When  datediff(day,B.GPAFECNAC,getdate())/365 >= 1 Then 'A'\n"
                + "			When datediff(day,B.GPAFECNAC,getdate())/365 < 1 Then 'M' END) as EDAD2,\n"
                + "\n"
                + "	(Case When  B.GPASEXPAC= 1 Then 'H'	 \n"
                + "			When  B.GPASEXPAC= 2 Then 'M' End) as SEXO,\n"
                + "\n"
                + "		(Case When  B.GPASEXPAC= 1 Then 'M'	 \n"
                + "			When  B.GPASEXPAC= 2 Then 'F' End) as GENERO,\n"
                + "	\n"
                + "	B.GPASEXPAC  AS VOLUNTAD_ANTICIPADA, --Modificar esto no es \n"
                + "	B.GPAFECNAC AS FECHA_VOLUNTAD_ANTICIPADA, ----Modificar esto no es\n"
                + "\n"
                + "	(Case When (B.GENOCUPACION) IS NULL  Then 'NA' ELSE ---Esto se deja asi pero deberia ser un join \n"
                + "				(SELECT OC.DGOCODIGO FROM  DGEMPRES60..GENOCUPACION AS OC WHERE B.GENOCUPACION=OC.OID) END ) AS CODIGO_OCUPACION,\n"
                + "	(Case When (B.GENOCUPACION) IS NULL  Then 'NO APLICA' ELSE ---Esto se deja asi pero deberia ser un join \n"
                + "				(SELECT OC.DGONOMBRE FROM  DGEMPRES60..GENOCUPACION AS OC WHERE B.GENOCUPACION=OC.OID) END ) AS NOMBRE_OCUPACION,\n"
                + "	--No se encuentra informacion para voluntad anticipada						\n"
                + "	B.GPCONDIC AS CODIGO_DISCAPACIDAD,\n"
                + "		(Case  When  ((B.GPCONDIC = 0 OR B.GPCONDIC = '' OR B.GPCONDIC=10)) Then 0 --'NINGUNA'\n"
                + "				When  B.GPCONDIC = 1 Then 1 --'DISCAPACIDAD FISICA'\n"
                + "				When  B.GPCONDIC = 2 Then 2 --'DISCAPACIDAD AUDITIVA'\n"
                + "				When  B.GPCONDIC = 3 Then 3 --'DISCAPACIDAD VISUAL'\n"
                + "				When  B.GPCONDIC = 4 Then 4 --'SORDOCEGUERA'\n"
                + "				When  B.GPCONDIC = 5 Then 5 --'DISCAPACIDAD INTELECTUAL'\n"
                + "				When  B.GPCONDIC = 6 Then 6 --'DISCPAPACIDAD PSICOSOCIAL (MENTAL)'\n"
                + "				When  B.GPCONDIC = 7 Then 7 --'DISCAPACIDAD MULTIPLE'\n"
                + "	End) DISCAPACIDAD,\n"
                + "	'COL' COD_PAIS_RES, 'COLOMBIA' NOMBRE_PAIS_RES,--RESOLVER ESTA VARIABLE CON EL CODIGO DEL PAIS\n"
                + "	--K.DEPCODDEP CODIGO_DEPTO_RES,M.MUNCODMUN CODIGO_MUNICIPIO_RES,K.DEPNOMDEP NOM_DEPARTAMENTO_RES,M.MUNNOMMUN NOM_MUNICIPIO_RES,\n"
                + "		rtrim(K.DEPCODDEP)+rtrim(M.MUNCODMUN) CODIGO_MUNICIPIO_RES,M.MUNNOMMUN NOM_MUNICIPIO_RES,\n"
                + "		0  ETNIA,--Se usa este parametro ya que en el sistema no se realiza la solicitud del registro (ADNGRUETN)\n"
                + "		'' COMUNIDAD_ETNIA,\n"
                + "	(Case   When B.GPAZONRES = 1 then  1 --'URBANO'\n"
                + "			When B.GPAZONRES = 2 then  2 --'CENTRO POBLADO'\n"
                + "			When B.GPAZONRES = 3 then  3  end) ZONA,--'AREA RURAL DISPERSA'\n"
                + "	J.GDECODIGO, --Código de administrador de plan de beneficios\n"
                + "	J.GDENOMBRE, --Nombre de administrador de plan de beneficios. \n"
                + "	(Case  When B.GPAESTCIV = 0  then  1--Ninguno\n"
                + "			When B.GPAESTCIV = 1  then  1--Soltero\n"
                + "			When B.GPAESTCIV = 2  then  2--Casado\n"
                + "			When B.GPAESTCIV = 3  then  6--Viudo\n"
                + "			When B.GPAESTCIV = 4  then  3--Union Libre\n"
                + "			When B.GPAZONRES = 5  then  4 end ) ESTADOCIVIL --Separado o Divorsiado(No existe en las opciones) \n"
                + "	 --INTO INFOCONTACTO_IO						\n"
                + "FROM DGEMPRES60..GENPACIEN B  \n"
                + "INNER JOIN DGEMPRES60..GENMUNICI M  ON (M.OID=B.DGNMUNICIPIO)\n"
                + "INNER JOIN DGEMPRES60..GENDEPTO K  ON (M.GENDEPTO=K.OID)\n"
                + "INNER JOIN DGEMPRES60..GENDETCON  J ON (J.OID=B.GENDETCON) \n"
                + "INNER JOIN DGEMPRES60..ADNINGRESO I ON (I.GENPACIEN=B.OID)\n"
                + "WHERE  I.OID=" + id, (ResultSet rs, int i) -> {
                    Identificacion identificacion = new Identificacion();
                    identificacion.setNumero(rs.getString("DOCUMENTO"));
                    identificacion.setTipo(rs.getString("IDENTIFICACION"));
                    Nacionalidad nacionalidad = new Nacionalidad();
                    nacionalidad.setCodigo(rs.getString("CODIGO_NACIONALIDAD"));
                    nacionalidad.setNombre(rs.getString("NACIONALIDAD"));
                    Nombres nombres = new Nombres();
                    nombres.setApellido1(rs.getString("APELLIDO1"));
                    nombres.setApellido2(rs.getString("APELLIDO2"));
                    nombres.setNombre1(rs.getString("NOMBRE1"));
                    nombres.setNombre2(rs.getString("NOMBRE2"));
                    EdadEstimada edadEstimada = new EdadEstimada();
                    edadEstimada.setUnidad(rs.getString("EDAD2"));
                    edadEstimada.setValor(rs.getInt("EDAD"));
                    VoluntadAnticipada voluntadAnticipada = new VoluntadAnticipada();
                    voluntadAnticipada.setFecha(rs.getDate("FECHA_VOLUNTAD_ANTICIPADA"));
                    voluntadAnticipada.setTipo(rs.getInt("VOLUNTAD_ANTICIPADA"));
                    Ocupacion ocupacion = new Ocupacion();
                    ocupacion.setCodigo(rs.getString("CODIGO_OCUPACION"));
                    ocupacion.setNombre(rs.getString("NOMBRE_OCUPACION"));
                    PaisResidencia paisResidencia = new PaisResidencia();
                    paisResidencia.setCodigo(rs.getString("COD_PAIS_RES"));
                    paisResidencia.setNombre(rs.getString("NOMBRE_PAIS_RES"));
                    MunicipioResidencia municipioResidencia = new MunicipioResidencia();
                    municipioResidencia.setCodigo(rs.getString("CODIGO_MUNICIPIO_RES"));
                    municipioResidencia.setNombre(rs.getString("NOM_MUNICIPIO_RES"));
                    Eps eps = new Eps();
                    eps.setCodigo(rs.getString("GDECODIGO"));
                    eps.setNombre(rs.getString("GDENOMBRE"));
                    IdentificationPatient personalInformation = new IdentificationPatient();
                    personalInformation.setNacionalidad(nacionalidad);
                    personalInformation.setIdentificacion(identificacion);
                    personalInformation.setNombres(nombres);
                    personalInformation.setFechaNacimiento(rs.getDate("GPAFECNAC"));
                    personalInformation.setEdadEstimada(edadEstimada);
                    personalInformation.setSexo(rs.getString("SEXO"));
                    personalInformation.setGenero(rs.getString("GENERO"));
                    personalInformation.setVoluntadAnticipada(voluntadAnticipada);
                    personalInformation.setOcupacion(ocupacion);
                    switch (rs.getInt("DISCAPACIDAD")) {
                        case 0:
                            personalInformation.setDiscapacidad("NINGUNA");
                            break;
                        case 1:
                            personalInformation.setDiscapacidad("DISCAPACIDAD FISICA");
                            break;
                        case 2:
                            personalInformation.setDiscapacidad("DISCAPACIDAD AUDITIVA");
                            break;
                        case 3:
                            personalInformation.setDiscapacidad("DISCAPACIDAD VISUAL");
                            break;
                        case 4:
                            personalInformation.setDiscapacidad("SORDOCEGUERA");
                            break;
                        case 5:
                            personalInformation.setDiscapacidad("DISCAPACIDAD INTELECTUAL");
                            break;
                        case 6:
                            personalInformation.setDiscapacidad("DISCAPACIDAD PSICOSOCIAL (MENTAL)");
                            break;
                        case 7:
                            personalInformation.setDiscapacidad("DISCAPACIDAD MULTIPLE");
                            break;
                    }
                    personalInformation.setPaisResidencia(paisResidencia);
                    personalInformation.setMunicipioResidencia(municipioResidencia);
                    personalInformation.setEtnia(rs.getString("ETNIA"));
                    switch (rs.getInt("ZONA")) {
                        case 1:
                            personalInformation.setZona("URBANO");
                            break;
                        case 2:
                            personalInformation.setZona("CENTRO POBLADO");
                            break;
                        case 3:
                            personalInformation.setZona("AREA RURAL DISPERSA");
                            break;
                    }
                    personalInformation.setEps(eps);
                    switch (rs.getInt("ESTADOCIVIL")) {
                        case 0:
                            personalInformation.setEstadoCivil("NINGUNO");
                            break;
                        case 1:
                            personalInformation.setEstadoCivil("SOLTERO");
                            break;
                        case 2:
                            personalInformation.setEstadoCivil("CASADO");
                            break;
                        case 3:
                            personalInformation.setEstadoCivil("VIUDO");
                            break;
                        case 4:
                            personalInformation.setEstadoCivil("UNION LIBRE");
                            break;
                        case 5:
                            break;
                    }
                    return personalInformation;
                });
        return identificationPatient;
    }
}
