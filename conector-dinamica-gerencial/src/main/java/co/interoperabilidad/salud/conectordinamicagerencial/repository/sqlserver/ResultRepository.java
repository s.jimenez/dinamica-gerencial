/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.interoperabilidad.salud.conectordinamicagerencial.repository.sqlserver;

import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.AntecedenteFamiliar;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.Diagnostico;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.DiagnosticoComplicacion;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.DiagnosticoMuerte;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.DiagnosticoRelacionado1;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.DiagnosticoRelacionado2;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.DiagnosticoRelacionado3;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.FactorRiesgo;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.PrestadorRefiere;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.Profesional;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.Result;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sebastian
 */
@Repository
public class ResultRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    private final List<Result> allInformationResult = new ArrayList<>();

    public List<Result> getResult(Integer id) {
        this.allInformationResult.clear();
        try {
            this.jdbcTemplate.queryForObject(
                    "SELECT 2 TIPO_DX,\n"
                    + " (SELECT DG.DIACODIGO FROM DGEMPRES60..ADNDIAEGR EG INNER JOIN DGEMPRES60..GENDIAGNO DG ON DG.OID = EG.DIACODIGO WHERE EG.OID = (SELECT MIN (OID)  FROM DGEMPRES60..ADNDIAEGR\n"
                    + "WHERE ADNEGRESO = E.OID AND Tipo = '5') and EG.ADNEGRESO = E.OID ) AS CODIGO_DX_EGRESO,\n"
                    + "(SELECT DG.DIANOMBRE FROM DGEMPRES60..ADNDIAEGR EG INNER JOIN DGEMPRES60..GENDIAGNO DG ON DG.OID = EG.DIACODIGO WHERE EG.OID = (SELECT MIN (OID)  FROM DGEMPRES60..ADNDIAEGR\n"
                    + "WHERE ADNEGRESO = E.OID AND Tipo = '5') and EG.ADNEGRESO = E.OID) AS DX_EGRESO,\n"
                    + "\n"
                    + "(SELECT DG.DIACODIGO FROM DGEMPRES60..ADNDIAEGR EG INNER JOIN DGEMPRES60..GENDIAGNO DG ON DG.OID = EG.DIACODIGO WHERE EG.OID = (SELECT MIN (OID) +1  FROM DGEMPRES60..ADNDIAEGR\n"
                    + "WHERE ADNEGRESO = E.OID) and EG.ADNEGRESO = E.OID) AS CODIGO_DX_RELA1,\n"
                    + "(SELECT DG.DIANOMBRE FROM DGEMPRES60..ADNDIAEGR EG INNER JOIN DGEMPRES60..GENDIAGNO DG ON DG.OID = EG.DIACODIGO WHERE EG.OID = (SELECT MIN (OID) +1  FROM DGEMPRES60..ADNDIAEGR\n"
                    + "WHERE ADNEGRESO = E.OID) and EG.ADNEGRESO = E.OID) AS DX_RELA1,\n"
                    + "\n"
                    + "\n"
                    + "(SELECT  DG.DIACODIGO FROM DGEMPRES60..ADNDIAEGR EG INNER JOIN DGEMPRES60..GENDIAGNO DG ON DG.OID = EG.DIACODIGO WHERE EG.OID = (SELECT MIN (OID) +2 FROM DGEMPRES60..ADNDIAEGR\n"
                    + "WHERE ADNEGRESO = E.OID) and EG.ADNEGRESO = E.OID) AS CODIGO_DX_RELA2,\n"
                    + "(SELECT   DG.DIANOMBRE FROM DGEMPRES60..ADNDIAEGR EG INNER JOIN DGEMPRES60..GENDIAGNO DG ON DG.OID = EG.DIACODIGO WHERE EG.OID = (SELECT MIN (OID) +2 FROM DGEMPRES60..ADNDIAEGR\n"
                    + "WHERE ADNEGRESO = E.OID) and EG.ADNEGRESO = E.OID) AS DX_RELA2,\n"
                    + "\n"
                    + "\n"
                    + "(SELECT DG.DIACODIGO FROM DGEMPRES60..ADNDIAEGR EG INNER JOIN DGEMPRES60..GENDIAGNO DG ON DG.OID = EG.DIACODIGO WHERE EG.OID = (SELECT MIN (OID) +3 FROM DGEMPRES60..ADNDIAEGR\n"
                    + "WHERE ADNEGRESO = E.OID) and EG.ADNEGRESO = E.OID) AS CODIGO_DX_RELA3,\n"
                    + "(SELECT DG.DIANOMBRE FROM DGEMPRES60..ADNDIAEGR EG INNER JOIN DGEMPRES60..GENDIAGNO DG ON DG.OID = EG.DIACODIGO WHERE EG.OID = (SELECT MIN (OID) +3 FROM DGEMPRES60..ADNDIAEGR\n"
                    + "WHERE ADNEGRESO = E.OID) and EG.ADNEGRESO = E.OID) AS DX_RELA3,\n"
                    + "\n"
                    + "NULL FECHA_RESULTADO,\n"
                    + "'' INSTRUMENTO_VALORACION,\n"
                    + "'' RESUL_PARAMETRO_VALOR,\n"
                    + "\n"
                    + "(SELECT DG.DIACODIGO FROM DGEMPRES60..ADNDIAEGR EG INNER JOIN DGEMPRES60..GENDIAGNO DG ON DG.OID = EG.DIACODIGO WHERE EG.OID = (SELECT MIN (OID) +4 FROM DGEMPRES60..ADNDIAEGR\n"
                    + "WHERE ADNEGRESO = E.OID) and EG.ADNEGRESO = E.OID) AS CODIGO_DX_COMPLICACION,\n"
                    + "(SELECT DG.DIANOMBRE FROM DGEMPRES60..ADNDIAEGR EG INNER JOIN DGEMPRES60..GENDIAGNO DG ON DG.OID = EG.DIACODIGO WHERE EG.OID = (SELECT MIN (OID) +4 FROM DGEMPRES60..ADNDIAEGR\n"
                    + "WHERE ADNEGRESO = E.OID) and EG.ADNEGRESO = E.OID) AS DX_COMPLICACION,\n"
                    + "\n"
                    + "E.ADEESTPAC CONDICION_USUARIO,\n"
                    + "\n"
                    + "	   (SELECT DG.DIACODIGO FROM DGEMPRES60..GENDIAGNO DG\n"
                    + "         WHERE DG.OID = E.DGNDIAGNO1) AS COD_DX_MUERTE,\n"
                    + "       (SELECT DG.DIANOMBRE FROM DGEMPRES60..GENDIAGNO DG\n"
                    + "      WHERE DG.OID = E.DGNDIAGNO1)AS DX_MUERTE,\n"
                    + "\n"
                    + "E.ADEFECSAL FECHA_FINALIZACION,\n"
                    + "'' PRESTADOR_REFIERE,\n"
                    + "\n"
                    + "(Case when HCNINCAPA.HCITIPINC is null then '' else HCNINCAPA.HCITIPINC end ) AS TIPO_INCAPACIDAD,\n"
                    + " \n"
                    + " (Case when HCNINCAPA.HCIDIASINC is null then '' else HCNINCAPA.HCIDIASINC end ) AS DIAS_INCAPACIDAD,\n"
                    + " null DIAS_LICENCIA_MATER,\n"
                    + "\n"
                    + " \n"
                    + "   (Case when (SELECT top 1 HCNANTECE.HCATIPANT  FROM DGEMPRES60..HCNANTECE where A.OID=HCNANTECE.GENPACIEN  AND HCNANTECE.HCATIPANT IN (6)) is null then ''\n"
                    + "     else (SELECT top 1 HCNANTECE.HCATIPANT  FROM DGEMPRES60..HCNANTECE where A.OID=HCNANTECE.GENPACIEN  AND HCNANTECE.HCATIPANT IN (6)) end) AS ANTECE_ALERGIA,\n"
                    + "\n"
                    + "	  (Case when (SELECT top 1 HCNANTECE.HCATIPANT  FROM DGEMPRES60..HCNANTECE where A.OID=HCNANTECE.GENPACIEN  AND HCNANTECE.HCATIPANT IN (6)) is null then ''\n"
                    + "     else (SELECT top 1 HCNANTECE.HCATIPANT  FROM DGEMPRES60..HCNANTECE where A.OID=HCNANTECE.GENPACIEN  AND HCNANTECE.HCATIPANT IN (11)) end) AS ANTECE_FAMILIAR,\n"
                    + "\n"
                    + "\n"
                    + "\n"
                    + "null PARENTESCO,\n"
                    + "null TIPO_RIESGO,\n"
                    + "\n"
                    + "( Case \n"
                    + "    When  A.PACTIPDOC = 1 Then 'CC'\n"
                    + "    When  A.PACTIPDOC= 2 Then 'CE'\n"
                    + "    When  A.PACTIPDOC= 3 Then 'TI'\n"
                    + "    When  A.PACTIPDOC= 4 Then 'RC'\n"
                    + "    When  A.PACTIPDOC= 5 Then 'PA'\n"
                    + "    When  A.PACTIPDOC= 6 Then 'AS'\n"
                    + "    When  A.PACTIPDOC= 7 Then 'MS'\n"
                    + "    When  A.PACTIPDOC= 8 Then 'NUI'\n"
                    + "    When  A.PACTIPDOC= 9 Then 'SC'\n"
                    + "    When  A.PACTIPDOC= 10 Then 'CN'\n"
                    + "    When  A.PACTIPDOC= 11 Then 'CD'\n"
                    + "    When  A.PACTIPDOC= 12 Then 'PE' End) As TIPO_DOCUMENTO,\n"
                    + "M.GMECODIGO DOCUMENTO\n"
                    + "\n"
                    + "\n"
                    + "FROM DGEMPRES60..GENPACIEN A\n"
                    + "\n"
                    + "INNER JOIN DGEMPRES60..ADNINGRESO D ON (A.OID=D.GENPACIEN)\n"
                    + "INNER JOIN DGEMPRES60..ADNEGRESO  E ON (D.OID=E.ADNINGRESO)\n"
                    + "INNER JOIN DGEMPRES60..HPNDEFCAM F ON (F.OID=D.HPNDEFCAM)\n"
                    + "INNER JOIN DGEMPRES60..GENDETCON G ON (G.OID=D.GENDETCON)\n"
                    + "inner join DGEMPRES60..GENMEDICO  M on (M.oid=E.GMECODIGO)\n"
                    + "INNER JOIN DGEMPRES60..ADNEGRESER J ON (J.OID=E.ADNEGRESER)\n"
                    + "LEFT JOIN DGEMPRES60..HCNINCAPA ON (HCNINCAPA.ADNINGRESO=D.OID)\n"
                    + "\n"
                    + "\n"
                    + "WHERE  D.OID=" + id, (ResultSet rs, int i) -> {
                        Diagnostico diagnostico = new Diagnostico();
                        diagnostico.setCodigo(rs.getString("CODIGO_DX_EGRESO"));
                        diagnostico.setNombre(rs.getString("DX_EGRESO"));
                        DiagnosticoRelacionado1 diagnosticoRelacionado1 = new DiagnosticoRelacionado1();
                        diagnosticoRelacionado1.setCodigo(rs.getString("CODIGO_DX_RELA1"));
                        diagnosticoRelacionado1.setNombre(rs.getString("DX_RELA1"));
                        DiagnosticoRelacionado2 diagnosticoRelacionado2 = new DiagnosticoRelacionado2();
                        diagnosticoRelacionado2.setCodigo(rs.getString("CODIGO_DX_RELA2"));
                        diagnosticoRelacionado2.setNombre(rs.getString("DX_RELA2"));
                        DiagnosticoRelacionado3 diagnosticoRelacionado3 = new DiagnosticoRelacionado3();
                        diagnosticoRelacionado3.setCodigo(rs.getString("CODIGO_DX_RELA3"));
                        diagnosticoRelacionado3.setNombre(rs.getString("DX_RELA3"));
                        DiagnosticoComplicacion diagnosticoComplicacion = new DiagnosticoComplicacion();
                        diagnosticoComplicacion.setCodigo(rs.getString("CODIGO_DX_COMPLICACION"));
                        diagnosticoComplicacion.setNombre(rs.getString("DX_COMPLICACION"));
                        DiagnosticoMuerte diagnosticoMuerte = new DiagnosticoMuerte();
                        diagnosticoMuerte.setCodigo(rs.getString("COD_DX_MUERTE"));
                        diagnosticoMuerte.setNombre(rs.getString("DX_MUERTE"));
                        PrestadorRefiere prestadorRefiere = new PrestadorRefiere();
                        prestadorRefiere.setCodigo(rs.getString("PRESTADOR_REFIERE"));
                        AntecedenteFamiliar antecedenteFamiliar = new AntecedenteFamiliar();
                        antecedenteFamiliar.setCondicion(rs.getInt("ANTECE_FAMILIAR"));
                        antecedenteFamiliar.setParentesco(rs.getInt("PARENTESCO"));
                        FactorRiesgo factorRiesgo = new FactorRiesgo();
                        factorRiesgo.setTipo(rs.getInt("TIPO_RIESGO"));
                        Profesional profesional = new Profesional();
                        profesional.setNumero(rs.getString("DOCUMENTO"));
                        profesional.setTipo(rs.getString("TIPO_DOCUMENTO"));
                        Result informationResult = new Result();
                        informationResult.setTipoDiagnostico(rs.getInt("TIPO_DX"));
                        informationResult.setDiagnostico(diagnostico);
                        informationResult.setDiagnosticoRelacionado1(diagnosticoRelacionado1);
                        informationResult.setDiagnosticoRelacionado2(diagnosticoRelacionado2);
                        informationResult.setDiagnosticoRelacionado3(diagnosticoRelacionado3);
                        informationResult.setFechaValoracion(rs.getDate("FECHA_RESULTADO"));
                        informationResult.setInstrumento(rs.getString("INSTRUMENTO_VALORACION"));
                        informationResult.setParametro(rs.getString("RESUL_PARAMETRO_VALOR"));
                        informationResult.setDiagnosticoComplicacion(diagnosticoComplicacion);
                        informationResult.setCondicion(rs.getInt("CONDICION_USUARIO"));
                        informationResult.setDiagnosticoMuerte(diagnosticoMuerte);
                        informationResult.setFechaFinalizacion(rs.getDate("FECHA_FINALIZACION"));
                        informationResult.setPrestadorRefiere(prestadorRefiere);
                        informationResult.setTipoIncapacidad(rs.getInt("TIPO_INCAPACIDAD"));
                        informationResult.setDiasIncapacidad(rs.getInt("DIAS_INCAPACIDAD"));
                        informationResult.setDiasMaternidad(rs.getInt("DIAS_LICENCIA_MATER"));
                        informationResult.setAntecedenteAlergias(rs.getInt("ANTECE_ALERGIA"));
                        informationResult.setAntecedenteFamiliar(antecedenteFamiliar);
                        informationResult.setFactorRiesgo(factorRiesgo);
                        informationResult.setProfesional(profesional);
                        this.allInformationResult.add(informationResult);
                        return this.allInformationResult;
                    });
        } catch (IncorrectResultSizeDataAccessException e) {
            System.out.println("Resultado finalizado");
        }
        
        return this.allInformationResult;
    }
}
