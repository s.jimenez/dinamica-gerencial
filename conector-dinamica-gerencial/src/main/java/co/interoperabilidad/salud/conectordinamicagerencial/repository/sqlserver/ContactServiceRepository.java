/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.interoperabilidad.salud.conectordinamicagerencial.repository.sqlserver;

import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.ContactServiceHealth;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.Diagnostico;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.Ips;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.TipoConsulta;
import java.sql.ResultSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sebastian
 */
@Repository
public class ContactServiceRepository {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    public ContactServiceHealth getContact(Integer id) {
        ContactServiceHealth contactServiceHealth = this.jdbcTemplate.queryForObject(
                "SELECT  					 \n"
                + "	I.AINFECING FECHA_INGRESO,\n"
                + "	(Case 	When I.AINTIPING = 1 Then 1 --'AMBULATORIO'\n"
                + "			When I.AINTIPING = 2 Then 2 --'HOSPITALARIO'\n"
                + "			End) As MODALIDAD, --Solo aparecen ambulatorio y hospitalario\n"
                + "		--Entorno no se encuentra variable\n"
                + "		--Via de ingreso al servicio de salud no se encuentra  variable\n"
                + "\n"
                + "	(Case 	When I.AINTIPRIE=0 then 0 --'NINGUNO'\n"
                + "			When I.AINTIPRIE=1 then 21 --'Accidente de transito'\n"
                + "			When I.AINTIPRIE=2 then 27 --'catastrofe'\n"
                + "			When I.AINTIPRIE=3 then 38 --'enfermedad general'\n"
                + "			When I.AINTIPRIE=4 then 21 --'accidente de trabajo'\n"
                + "			When I.AINTIPRIE=5 then 38 --'enfermedad general y maternidad'(Validar dudas de la clasificacion)\n"
                + "			When I.AINTIPRIE=6 then 37 --'atencion inicial de urgencias' (validar dudas de la clasificacion)\n"
                + "			When I.AINTIPRIE=7 then 26 --'otro tipo de accidente'\n"
                + "			When I.AINTIPRIE=8 then 28 --'lesion por agresion'\n"
                + "			When I.AINTIPRIE=9 then 29 --'lesion autoinfligida'\n"
                + "			When I.AINTIPRIE=10 then 30 --'maltrato fisico '\n"
                + "			When I.AINTIPRIE=11 then 40 --'promocion y prevencion'\n"
                + "			When I.AINTIPRIE=12 then 25 --'otro'\n"
                + "			When I.AINTIPRIE=13 then 25 --'accidente rabico' (validar dudas de la clasificacion)\n"
                + "			When I.AINTIPRIE=14 then 25 --'accidente ofidico' (validar dudas de la clasificacion)\n"
                + "			When I.AINTIPRIE=15 then 32 --'sospecha de abuso sexual'\n"
                + "			When I.AINTIPRIE=16 then 30 --'sospecha de violencia sexual'\n"
                + "			When I.AINTIPRIE=17 then 31 End ) CAUSA_ATENCION, --'sospecha de maltrato emocional'\n"
                + "	TR.HCTFECTRI FECHA_TRIAGE,\n"
                + "	-- CL.HCCODIGO MODALIDAD1,\n"
                + "	CL.HCCODIGO CLASIFICACION_TRIAGE,\n"
                + "	DX.DIACODIGO CODIGO_DIAGNOSTICO,\n"
                + "	DX.DIANOMBRE NOMBRE_DIAGNOSTICO,\n"
                + "	1 IMPRESION_DIAGNOSTICA,\n"
                + "	J.GDECODIGO AS IPS, ---'660010076201' IPS,\n"
                + "	J.GDENOMBRE AS NOMBRE_IPS,\n"
                + "	(Case 	When I.AINURGCON=-1 then 'NINGUNO'\n"
                + "			When I.AINURGCON=0 then 'URGENCIAS'  \n"
                + "			When I.AINURGCON=1 then 'CONSULTA EXTERNA'  \n"
                + "			When I.AINURGCON=2 then 'NACIDO EN EL HOSPITAL'\n"
                + "			When I.AINURGCON=3 then 'REMITIDO'  \n"
                + "			When I.AINURGCON=4 then 'HOSPITALIZACIÓN DE URGENCIAS' \n"
                + "			When I.AINURGCON=5 then 'HOSPITALIZACIÓN'  \n"
                + "			When I.AINURGCON=6 then 'IMÁGENES'  \n"
                + "			When I.AINURGCON=7 then 'LABORATORIO'  \n"
                + "			When I.AINURGCON=8 then 'URGENCIA GINECOLÓGICA'  \n"
                + "			When I.AINURGCON=9 then 'QUIRÓFANO'  \n"
                + "			When I.AINURGCON=10 then 'CIRUGÍA AMBULATORIA'  \n"
                + "			When I.AINURGCON=11 then 'CIRUGÍA PROGAMADA'  \n"
                + "			When I.AINURGCON=12 then 'UCI NEONATAL'  \n"
                + "			When I.AINURGCON=13 then 'UCI ADULTO' End ) TIPO_ATENCION,\n"
                + "     I.AINURGCON AS CODIGO_ATENCION,"
                + "	(Case 	When I.GPATIPPAC =0  Then '' --'NINGUNO'\n"
                + "			When I.GPATIPPAC =1  Then 'C' --'CONTRIBUTIVO'\n"
                + "			When I.GPATIPPAC =2  Then 'S' --'SUBSIDIADO'\n"
                + "			When I.GPATIPPAC =3  Then 'V' --'VINCULADO'\n"
                + "			When I.GPATIPPAC =4  Then 'P' --'PARTICULAR'\n"
                + "			When I.GPATIPPAC =5  Then 'O' --'OTRO'\n"
                + "			When I.GPATIPPAC =6  Then 'C' --'DESPLAZADO REGIMEN CONTRIBUTIVO'\n"
                + "			When I.GPATIPPAC =7  Then 'S' --'DESPLAZADO REGIMEN SUBSIDIADO'\n"
                + "			When I.GPATIPPAC =8  Then 'O' End) As TIPO_USUARIO, --'DESPLAZADO NO ASEGURADO'\n"
                + "	(Case 	When B.GPATIPAFI=0 then '' --'NINGUNO'\n"
                + "			When B.GPATIPAFI=1 then 'C' --'COTIZANTE'  \n"
                + "			When B.GPATIPAFI=2 then 'B' --'BENEFICIARIO'\n"
                + "			When B.GPATIPAFI=3 then 'A' --'ADICIONAL'  \n"
                + "			When B.GPATIPAFI=4 then 'C' --'JUBILADO RETIRADO' \n"
                + "			When B.GPATIPAFI=5 then 'C' End ) TIPO_AFILIADO, --'PENSIONADO'\n"
                + "		--Condicion de la usuario No se identifica la variable\n"
                + "		I.AINMOTCON ENFERMEDAD_ACTUAL\n"
                + "FROM DGEMPRES60..GENPACIEN B  \n"
                + "INNER JOIN DGEMPRES60..GENDETCON  J ON (J.OID=B.GENDETCON) \n"
                + "INNER JOIN DGEMPRES60..GENCONTRA  C ON (C.OID=J.GENCONTRA1) \n"
                + "INNER JOIN DGEMPRES60..GEENENTADM  E ON (E.OID=C.DGNENTADM1) \n"
                + "INNER JOIN DGEMPRES60..ADNINGRESO I ON (I.GENPACIEN=B.OID)\n"
                + "LEFT JOIN  DGEMPRES60..HCNTRIAGE TR on I.HCENTRIAGE = TR.OID\n"
                + "LEFT JOIN DGEMPRES60..HCNCONTRDT CT on TR.HCNCONTRDT = CT.OID\n"
                + "LEFT JOIN DGEMPRES60..HCNCLAURGTR CL on TR.HCNCLAURGTR = CL.OID\n"
                + "LEFT JOIN DGEMPRES60..GENDIAGNO DX On DX.OID = I.DGNDIAGNO \n"
                + "WHERE  I.OID=" + id, (ResultSet rs, int i) -> {
                    Diagnostico diagnostico = new Diagnostico();
                    diagnostico.setCodigo(rs.getString("CODIGO_DIAGNOSTICO"));
                    diagnostico.setNombre(rs.getString("NOMBRE_DIAGNOSTICO"));
                    Ips ips = new Ips();
                    ips.setCodigo(rs.getString("IPS"));
                    ips.setNombre(rs.getString("NOMBRE_IPS"));
                    TipoConsulta tipoConsulta = new TipoConsulta();
                    tipoConsulta.setCodigo(rs.getInt("CODIGO_ATENCION"));
                    tipoConsulta.setNombre("TIPO_ATENCION");
                    ContactServiceHealth informationContact = new ContactServiceHealth();
                    informationContact.setFechaHora(rs.getDate("FECHA_INGRESO"));
                    informationContact.setModalidad(rs.getInt("MODALIDAD"));
                    informationContact.setCausa(rs.getInt("CAUSA_ATENCION"));
                    informationContact.setFechaTriage(rs.getDate("FECHA_TRIAGE"));
                    informationContact.setClasificacion(rs.getString("CLASIFICACION_TRIAGE"));
                    informationContact.setDiagnostico(diagnostico);
                    informationContact.setTipoDiagnostico(rs.getInt("IMPRESION_DIAGNOSTICA"));
                    informationContact.setIps(ips);
                    informationContact.setTipoConsulta(tipoConsulta);
                    informationContact.setTipoUsuario(rs.getString("TIPO_USUARIO"));
                    informationContact.setTipoAfiliado(rs.getString("TIPO_AFILIADO"));
                    informationContact.setEnfermedadActual("ENFERMEDAD_ACTUAL");
                    return informationContact;
                }
        );
        return contactServiceHealth;
    }
}
