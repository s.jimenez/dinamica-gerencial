package co.interoperabilidad.salud.conectordinamicagerencial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConectorDinamicaGerencialApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConectorDinamicaGerencialApplication.class, args);
    }

}
