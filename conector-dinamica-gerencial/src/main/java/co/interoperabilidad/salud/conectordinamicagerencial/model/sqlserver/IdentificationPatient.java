/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Sebastian
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class IdentificationPatient {
    
    private Comunidad comunidad;
    private String discapacidad;
    private EdadEstimada edadEstimada;
    private Eps eps;
    private String estadoCivil;
    private String etnia;
    private Date fechaNacimiento;
    private String genero;
    private Identificacion identificacion;
    private MunicipioResidencia municipioResidencia;
    private Nacionalidad nacionalidad;
    private Nombres nombres;
    private Ocupacion ocupacion;
    private PaisResidencia paisResidencia;
    private String sexo;
    private VoluntadAnticipada voluntadAnticipada;
    private String zona;
    
}
