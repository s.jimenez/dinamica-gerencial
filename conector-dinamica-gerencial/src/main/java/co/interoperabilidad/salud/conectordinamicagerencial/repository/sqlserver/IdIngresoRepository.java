/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.interoperabilidad.salud.conectordinamicagerencial.repository.sqlserver;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.IdIngreso;
import java.sql.ResultSet;
import org.springframework.dao.IncorrectResultSizeDataAccessException;

/**
 *
 * @author Sebastian
 */
@Repository
public class IdIngresoRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    private final List<IdIngreso> allInformationId = new ArrayList<>();

    public List<IdIngreso> getListId(Integer id) {
        this.allInformationId.clear();
        try {
            return this.jdbcTemplate.queryForObject(
                    "SELECT ADNINGRESO FROM DGEMPRES60..ADNEGRESO WHERE ADNINGRESO>=" + id,
                    (ResultSet rs, int i) -> {
                        IdIngreso idIngreso = new IdIngreso();
                        idIngreso.setId(rs.getInt("ADNINGRESO"));
                        this.allInformationId.add(idIngreso);
                        return this.allInformationId;
                    });
        } catch (IncorrectResultSizeDataAccessException e) {
            System.out.println("Id finalizado");
        }
        return this.allInformationId;
    }
}
