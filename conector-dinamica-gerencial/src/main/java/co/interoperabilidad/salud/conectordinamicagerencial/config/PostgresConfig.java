/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.interoperabilidad.salud.conectordinamicagerencial.config;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author Sebastian
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "postgresEntityManagerFactory",
        transactionManagerRef = "postgresTransactionManager", basePackages = {
            "co.interoperabilidad.salud.conectordinamicagerencial.repository.pg"
        })
public class PostgresConfig {
    
    @Autowired
    private Environment env;
    
    @Bean(name = "postgresDataSource")
    public DataSource postgresDataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("legacy.datasource.driver-class-name"));
        dataSource.setUrl(env.getProperty("legacy.datasource.url"));
        dataSource.setUsername(env.getProperty("legacy.datasource.username"));
        dataSource.setPassword(env.getProperty("legacy.datasource.password"));

        return dataSource;
    }
    
    @Bean(name = "postgresEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(postgresDataSource());
        em.setPackagesToScan("co.interoperabilidad.salud.conectordinamicagerencial.model.pg");
        
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        
        Map<String, Object> properties = new HashMap<>();
        properties.put("hibernate.show-sql", env.getProperty("legacy.jpa.show_sql"));
        properties.put("hibernate.dialect", env.getProperty("legacy.jpa.database-platform"));
        properties.put("hibernate.hbm2ddl.auto", env.getProperty("legacy.jpa.ddl-auto"));
        properties.put("hibernate.jdbc.lob.non_contextual_creation",
                env.getProperty("legacy.jpa.properties.hibernate.jdbc.lob.non_contextual_creation"));
        properties.put("hibernate.generate-ddl", env.getProperty("leagcy.jpa.generate-ddl"));
        
        em.setJpaPropertyMap(properties);
        
        return em;
    }
    
    @Bean(name = "postgresTransactionManager")
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        
        return transactionManager;
    }
}
