/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Sebastian
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class ContactServiceHealth {
    
    private Integer causa;
    private String clasificacion;
    private Integer condicionUsuaria;
    private Diagnostico diagnostico;
    private String enfermedadActual;
    private Integer entorno;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date fechaHora;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date fechaTriage;
    private Ips ips;
    private Integer modalidad;
    private String tipoAfiliado;
    private TipoConsulta tipoConsulta;
    private Integer tipoDiagnostico;
    private String tipoUsuario;
    private Integer viaIngreso;
    
}
