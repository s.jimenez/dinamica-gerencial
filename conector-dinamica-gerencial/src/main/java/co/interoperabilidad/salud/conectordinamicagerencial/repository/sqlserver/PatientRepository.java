/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.interoperabilidad.salud.conectordinamicagerencial.repository.sqlserver;

import org.springframework.stereotype.Repository;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.Patient;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Sebastian
 */
@Repository
public class PatientRepository {

    @Autowired
    private IdentificationPatientRepository identificationPatientRepository;
    @Autowired
    private HealthTechnologyRepository healthTechnologyRepository;
    @Autowired
    private ContactServiceRepository contactServiceRepository;
    @Autowired
    private ResultRepository resultRepository;

    public Patient getPatientById(Integer id) {
        Patient patient = new Patient();
        patient.setContacto(this.contactServiceRepository.getContact(id));
        patient.setInformacion(this.identificationPatientRepository.getInfoContacto(id));
        patient.setResultados(this.resultRepository.getResult(id));
        patient.setTecnologias(this.healthTechnologyRepository.getTechnology(id));
        return patient;
    }
}
