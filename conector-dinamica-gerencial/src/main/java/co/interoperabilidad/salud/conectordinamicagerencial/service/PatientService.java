/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.interoperabilidad.salud.conectordinamicagerencial.service;

import co.interoperabilidad.salud.conectordinamicagerencial.repository.sqlserver.PatientRepository;
import org.springframework.stereotype.Service;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.Patient;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.IdIngreso;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.TokenAuth;
import co.interoperabilidad.salud.conectordinamicagerencial.repository.pg.LastIdRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import co.interoperabilidad.salud.conectordinamicagerencial.model.pg.LastId;
import co.interoperabilidad.salud.conectordinamicagerencial.repository.sqlserver.IdIngresoRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Sebastian
 */
@Service
public class PatientService {

    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private IdIngresoRepository idIngresoRepository;
    @Autowired
    private LastIdRepository lastIdRepository;
    @Autowired
    private RestTemplate restTemplate;
    @Value("${dinamica.port}")
    private String serverPort;
    @Value("${dinamica.address}")
    private String serverAddress;
    private String authToken;

    public Patient getPatientById(Integer id) {
        Patient patient = this.patientRepository.getPatientById(id);
        lastIdRepository.save(this.sendLastId(id));
        return patient;
    }

    public LastId getLastId() {
        List<LastId> consultAllId = (List<LastId>) this.lastIdRepository.findAll();
        if (consultAllId.isEmpty()) {
            Integer id = 838962;
            List<IdIngreso> idIngreso = this.idIngresoRepository.getListId(id);
            LastId newId = new LastId();
            newId.setId(idIngreso.get((idIngreso.size() - idIngreso.size()) + 1).getId());
            this.lastIdRepository.save(newId);
            return newId;
        } else {
            LastId savedId = consultAllId.get(consultAllId.size() - 1);
            List<IdIngreso> idIngreso = this.idIngresoRepository.getListId(savedId.getId());
            LastId newId = new LastId();
            newId.setId(idIngreso.get((idIngreso.size() - idIngreso.size()) + 1).getId());
            this.lastIdRepository.save(newId);
            return newId;
        }
    }

    public Patient getPatientByLastId() {
        LastId savedId = this.getLastId();
        Patient savedPatient = this.patientRepository.getPatientById(savedId.getId());
        return savedPatient;
    }

    public List<LastId> findAllId() {
        List<LastId> consultAllId = (List<LastId>) this.lastIdRepository.findAll();
        return consultAllId;
    }

    public LastId sendLastId(Integer id) {
        LastId lastId = new LastId();
        lastId.setId(id);
        lastIdRepository.save(lastId);
        return lastId;
    }

    public ResponseEntity<TokenAuth> autenticate(String username, String password) {
        ResponseEntity<TokenAuth> response = restTemplate.postForEntity(
                "http://"
                + serverAddress + ":" + serverPort
                + "/his/autenticate?username=" + username + "&password=" + password, null,
                TokenAuth.class);
        TokenAuth tokenAuth = response.getBody();
        authToken = tokenAuth.getAccess_token();
        if (response.getStatusCode() == HttpStatus.OK) {
            return ResponseEntity.status(HttpStatus.CREATED).body(response.getBody());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity<Patient> sendEvent() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + authToken);
        Patient patientEvent = this.getPatientByLastId();
        HttpEntity<Patient> entity = new HttpEntity<>(patientEvent, headers);
        ResponseEntity<Patient> response = restTemplate.postForEntity(
                "http://"
                + serverAddress + ":" + serverPort
                + "/his/api/v1/eventos", entity,
                Patient.class);
        if (response.getStatusCode() == HttpStatus.OK) {
            return ResponseEntity.status(HttpStatus.CREATED).body(response.getBody());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

}
