/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Sebastian
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class HealthTechnology {
    
    private Integer cantidad;
    private String dosis;
    private Integer duracion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date fechaHora;
    private String finalidad;
    private String frecuencia;
    private Medicamento medicamento;
    private Profesional profesional;
    private Tecnologia tecnologia;
    private String tipo;
    private String via;
    
}
