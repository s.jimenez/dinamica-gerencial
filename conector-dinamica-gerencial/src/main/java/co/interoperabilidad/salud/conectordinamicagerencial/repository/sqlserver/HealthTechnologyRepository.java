/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.interoperabilidad.salud.conectordinamicagerencial.repository.sqlserver;

import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.HealthTechnology;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.Tecnologia;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.Medicamento;
import co.interoperabilidad.salud.conectordinamicagerencial.model.sqlserver.Profesional;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sebastian
 */
@Repository
public class HealthTechnologyRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    private final List<HealthTechnology> allInformationTechnology = new ArrayList<>();

    public List<HealthTechnology> getTechnology(Integer id) {
        this.allInformationTechnology.clear();
        try {
            return this.jdbcTemplate.queryForObject(
                    "SELECT  \n"
                    + "  --Tipo de tecnologia no se encuentra\n"
                    + "HCNMEDPAC.HCNFECCANTID FECHA_TECNOLOGIA,\n"
                    + "INNPRODUC.IPRCUM TIPO_TECNOLOGIA,\n"
                    + "INNPRODUC.IPRDESCOR NOMBRE_TECNOLOGIA ,\n"
                    + "'15' FINALIDAD, --No se encuentra la variable con las opciones en el diccionario\n"
                    + "INNPRODUC.IPRFORFAR as FORMA_FARMACEUTICA ,\n"
                    + "INNPRODUC.IPRCONCEN as CONCENTRACION,\n"
                    + "INNPRODUC.IUNCODIGO as UNIDAD_MEDIDA,\n"
                    + "HCNMEDPAC.HCSOBSERV AS DOSIS,\n"
                    + "(Case  When HCNMEDPAC.HCSVIAADM = -1 Then  'Ninguno'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  0 Then  'Oral'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  1 Then  'Oral_Sonda'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  2 Then  'Oral_Succion'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  3 Then  'Endovenosa'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  4 Then  'Intramuscular'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  5 Then  'Subcutaneo'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  6 Then  'Topico'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  7 Then  'Infusión'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  8 Then  'Nutricion_Enteral'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  9 Then  'Nutricion_Parenteral'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  10 Then 'Inhalatoria'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  11 Then  'Intrarrectal'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  12 Then  'Transdérmica'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  13 Then  'Sublingual'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  14 Then  'Vaginal'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  15 Then  'Oftálmica'\n"
                    + "    When HCNMEDPAC.HCSVIAADM =  16  Then  'Ótica'  End) VIA_ADMINNISTRACION,\n"
                    + "HCNMEDPAC.HCSFRECMED AS FRECUENCIA,\n"
                    + "HCNMEDPAC.HCSTERDEF AS DIAS_TRATAMIENTO,\n"
                    + "HCNMEDPAC.HCSCANTI UNIDADES_ENTREGADAS,\n"
                    + "(Case\n"
                    + "    When GENTERCER.TERTIPDOC = 1 Then 'CC'\n"
                    + "    When GENTERCER.TERTIPDOC = 2 Then 'CE'\n"
                    + "    When GENTERCER.TERTIPDOC = 3 Then 'TI'\n"
                    + "    When GENTERCER.TERTIPDOC = 4 Then 'RC'\n"
                    + "    When GENTERCER.TERTIPDOC = 5 Then 'PA'\n"
                    + "    When GENTERCER.TERTIPDOC = 6 Then 'AS'\n"
                    + "    When GENTERCER.TERTIPDOC = 7 Then 'MS'\n"
                    + "    When GENTERCER.TERTIPDOC = 8 Then 'NV'\n"
                    + "    When GENTERCER.TERTIPDOC = 9 Then 'NIT'\n"
                    + "  End) As TIPO_DOCUMENTO,\n"
                    + "  GENUSUARIO.USUNOMBRE DOCUMENTO\n"
                    + "\n"
                    + " from INNMSUMPA\n"
                    + "      inner join INNCSUMPA  on (INNCSUMPA.OID = INNMSUMPA.INNCSUMPA)\n"
                    + "      inner join ADNINGRESO on (ADNINGRESO.OID = INNCSUMPA.ADNINGRESO)\n"
                    + "      inner join GENPACIEN on (GENPACIEN.OID = ADNINGRESO.GENPACIEN)\n"
                    + "      inner join HCNMEDPAC on (HCNMEDPAC.OID = INNMSUMPA.HCNMEDPAC)\n"
                    + "      inner join INNPRODUC on (INNPRODUC.OID = INNMSUMPA.INNPRODUC)\n"
                    + "      inner join GENUSUARIO on (GENUSUARIO.OID = HCNMEDPAC.GENUSUARIOCNT)\n"
                    + "      inner join HPNDEFCAM on (HPNDEFCAM.OID = ADNINGRESO.HPNDEFCAM)\n"
                    + "      inner join GENMEDICO on (GENUSUARIO.oid=GENMEDICO.GENUSUARIO)\n"
                    + "      inner join GENTERCER   on (GENTERCER.OID = GENMEDICO.GENTERCER)\n"
                    + "\n"
                    + "WHERE ADNINGRESO.OID = 838963"
                    + "\n union all\n"
                    + "\n"
                    + "--EXAMENES SOLICITADO\n"
                    + "select \n"
                    + "hcnsolexa.hcsfecsol FECHA_TECNOLOGIA,\n"
                    + "genserips.SIPCODCUP TIPO_TECNOLOGIA,\n"
                    + "genserips.sipnombre NOMBE_TECNOLOGIA,\n"
                    + "'16' FINALIDAD,\n"
                    + "'' FORMA_FARMACEUTICA,\n"
                    + "'' CONCENTRACION,\n"
                    + "'' UNIDAD_MEDIDA,\n"
                    + "'' DOSIS,\n"
                    + "'' VIA_ADMINISTRACION,\n"
                    + "'' FRECUENCIA,\n"
                    + "'' DIAS_TRATAMIENTO,\n"
                    + "hcnsolexa.HCSCANTI UNIDADES_ENTREGADAS,\n"
                    + "(Case\n"
                    + "  When gentercer.tertipdoc = 1 Then 'CC'\n"
                    + "  When gentercer.tertipdoc = 2 Then 'CE'\n"
                    + "  When gentercer.tertipdoc = 3 Then 'TI'\n"
                    + "  When gentercer.tertipdoc = 4 Then 'RC'\n"
                    + "  When gentercer.tertipdoc = 5 Then 'PA'\n"
                    + "  When gentercer.tertipdoc = 6 Then 'AS'\n"
                    + "  When gentercer.tertipdoc = 7 Then 'MS'\n"
                    + "  When gentercer.tertipdoc = 8 Then 'NV'\n"
                    + "  When gentercer.tertipdoc = 9 Then 'NIT'\n"
                    + "End) As TIPO_DOCUMENTO,\n"
                    + "genmedico.GMECODIGO DOCUMENTO\n"
                    + "from hcnsolexa \n"
                    + "  inner join genserips  on (genserips.oid = hcnsolexa.genserips)  \n"
                    + "  inner join adningreso on (adningreso.oid = hcnsolexa.adningreso ) \n"
                    + "  inner join hcnfolio   on (hcnfolio.oid = hcnsolexa.hcnfolio)\n"
                    + "  inner join genpacien  on (genpacien.oid = hcnfolio.genpacien)\n"
                    + "  inner join genareser  on (genareser.oid = hcnfolio.genareser)\n"
                    + "  inner join genmedico  on (genmedico.oid=hcnfolio.genmedico)\n"
                    + "  inner join gentercer  on (gentercer.oid=genmedico.gentercer)\n"
                    + "WHERE ADNINGRESO.OID =" + id, (ResultSet rs, int i) -> {
                        Tecnologia tecnologia = new Tecnologia();
                        tecnologia.setCodigo(rs.getString("TIPO_TECNOLOGIA"));
                        tecnologia.setNombre(rs.getString("NOMBRE_TECNOLOGIA"));
                        Medicamento medicamento = new Medicamento();
                        medicamento.setForma(rs.getString("FORMA_FARMACEUTICA"));
                        medicamento.setConcentracion(rs.getString("CONCENTRACION"));
                        medicamento.setUnidad(rs.getInt("UNIDAD_MEDIDA"));
                        Profesional profesional = new Profesional();
                        profesional.setTipo(rs.getString("TIPO_DOCUMENTO"));
                        profesional.setNumero(rs.getString("DOCUMENTO"));
                        HealthTechnology informationTechnology = new HealthTechnology();
                        informationTechnology.setFechaHora(rs.getDate("FECHA_TECNOLOGIA"));
                        informationTechnology.setTecnologia(tecnologia);
                        informationTechnology.setFinalidad("FINALIDAD");
                        informationTechnology.setMedicamento(medicamento);
                        informationTechnology.setDosis(rs.getString("DOSIS"));
                        informationTechnology.setVia(rs.getString("VIA_ADMINNISTRACION"));
                        informationTechnology.setFrecuencia(rs.getString("FRECUENCIA"));
                        informationTechnology.setDuracion(rs.getInt("DIAS_TRATAMIENTO"));
                        informationTechnology.setCantidad(rs.getInt("UNIDADES_ENTREGADAS"));
                        informationTechnology.setProfesional(profesional);
                        this.allInformationTechnology.add(informationTechnology);
                        return this.allInformationTechnology;
                    });
        } catch (IncorrectResultSizeDataAccessException e) {
            System.out.println("Tecnologia finalizada");
        }
        
        return this.allInformationTechnology;
    }
}
